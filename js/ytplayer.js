
// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');
tag.src = "http://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubePlayerAPIReady() {

}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
	event.target.playVideo();
	var currentvid = playlist.get('currentVideo');
	$('#playlist tr#' + currentvid).css('background', '#DAFFC2');
}
// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
function onPlayerStateChange(event) {
	if (event.data == YT.PlayerState.ENDED) {
		var newVideo = playlist.get('currentVideo');
		$('#playlist tr#' + newVideo).css('background',  '#FFF');
		if (playlist.get('currentVideo') >= playlist.playlistCollection.models.length - 1) {
			newVideo = 0;
		} else {
			newVideo++;
		}
		playlist.set({currentVideo : newVideo});
	} else if (event.data == YT.PlayerState.PLAYING) {
		var currentvid = playlist.get('currentVideo');
		$('#playlist tr#' + currentvid).css('background', '#DAFFC2');
	}
}
function stopVideo() {
	player.stopVideo();
}