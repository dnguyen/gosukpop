<?php
	include('../inc/ExtractedVideo.php');
	include("../../../gosuconfig/config.php");

	$videoid = $_POST['v'];

	for ($i = 0; $i < count($_SESSION['playlist']); $i++) {
		$id = $_SESSION['playlist'][$i]->getId();
		if ($id == $videoid) {

			if ($_SESSION['loggedin']) {
				$sql = "DELETE FROM playlist_videos WHERE playlistid = ? AND videoid = ?";
				$query = $DBH->prepare($sql);
				$query->execute(array($_SESSION['current_playlist'], $videoid));
			} else {
				array_unshift($_SESSION['videoList'], $_SESSION['playlist'][$i]);
			}

			unset($_SESSION['playlist'][$i]);
			$_SESSION['playlist'] = array_values($_SESSION['playlist']);
			
			return true;
		}
	}

	return false;
?>