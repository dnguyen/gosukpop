<?php
	include("../../../gosuconfig/config.php");

	$response = array();

	if ($_SESSION['loggedin']) {
		if (!isset($_SESSION['playlist_list']))
			$_SESSION['playlist_list'] = array();
		if (preg_match("/^[A-Za-z0-9_]{3,20}$/", $_POST['name'])) {
			$sql = "INSERT INTO playlists(accountid, name) VALUES (?, ?)";
			$query = $DBH->prepare($sql);
			if ($query->execute(array($_SESSION['id'], $_POST['name']))) {

				$id = $DBH->lastInsertId();
				array_push($_SESSION['playlist_list'], array('id' => $id, 'name' => $_POST['name']));

				$response = array (
					'success' => true,
					'message' => "Added playlist",
					'id' => $id,
					'name' => $_POST['name']
				);
			}
		} else {
			$response = array(
				'success' => false,
				'message' => 'Can only contain 3 to 20 letters, numbers, or underscores'
			);
		}
	} else {
		$response = array (
			'success' => false,
			'message' => "Not logged in"
		);
	}

	echo json_encode($response);
?>