<?php
	include('../inc/ExtractedVideo.php');
	include("../../../gosuconfig/config.php");

	$videoid = $_POST['v'];

	for ($i = 0; $i < count($_SESSION['videoList']); $i++) {
		$id = $_SESSION['videoList'][$i]->getId();
		if ($id == $videoid) {
			array_push($_SESSION['playlist'], $_SESSION['videoList'][$i]);
			if ($_SESSION['loggedin']) {
				$sql = "INSERT INTO playlist_videos (playlistid, videoid) VALUES (?, ?)";
				$query = $DBH->prepare($sql);
				$query->execute(array($_SESSION['current_playlist'], $videoid));
			} else {
				unset($_SESSION['videoList'][$i]);
				$_SESSION['videoList'] = array_values($_SESSION['videoList']);
			}
			return true;
		}
	}

	return false;
?>