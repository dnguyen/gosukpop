<?php
    include('../inc/ExtractedVideo.php');
	include('../inc/VideoList.php');
	include("../../../gosuconfig/config.php");
    
	if (!isset($_SESSION['playlist']))
		$_SESSION['playlist'] = array();

    if ($_SESSION['loggedin']) {
    	if ($_GET['change']) {
    		$_SESSION['current_playlist'] = $_GET['id'];
		
			// Remove any previous videos that were in the playlist.
			unset($_SESSION['playlist']);
			$_SESSION['playlist'] = array();

			// Search for any videos in the playlist in the database.
    		$videosToAdd = array();
    		$sql = "SELECT * FROM playlist_videos WHERE playlistid= ?";
    		$query = $DBH->prepare($sql);
    		$query->execute(array($_GET['id']));
    		
    		while ($row = $query->fetch()) {
    			$videosToAdd[] = $row['videoid'];
    		}
    		// Get video data from main video list session.
    		if (count($videosToAdd) > 0) {
	    		for ($j = 0; $j < count($videosToAdd); $j++) {
		    		for ($i = 0; $i < count($_SESSION['videoList']); $i++) {
						if ($videosToAdd[$j] === $_SESSION['videoList'][$i]->getId()) {
							$_SESSION['playlist'][] = $_SESSION['videoList'][$i];
		    			}
		    		}
	    		}
    		}
            
    		echo json_encode(array(    				
	    			"current_playlist" => $_SESSION['current_playlist'],
    				"playlist" => $_SESSION['playlist']
	    	));

    	// Return the current playlist; the playlist hasn't changed.	
    	} else {
			echo json_encode(array('playlist' => $_SESSION['playlist']));
    	}

    // Not logged in. Just use regular playlist from session.	
    } else {
		echo json_encode(array('playlist' => $_SESSION['playlist']));
	}

?>