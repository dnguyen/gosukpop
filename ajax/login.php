<?php
	include("../../../gosuconfig/config.php");

	if (!isset($_SESSION['loggedin'])) {
		$_SESSION['playlist'] = array();
		
		$username = $_POST['data']['username'];
		$password = $_POST['data']['password'];

		$foundUser = false;
		$accountid = 0;
		$response = array();

		if ($username === '') {
			$response = array(
				'success' => false,
				'message' => 'Wrong username and password combination.'
			);
			echo json_encode($response);
			return;
		}

		if ($password === '') {
			$response = array(
				'success' => false,
				'message' => 'Wrong username and password combination.'
			);
			echo json_encode($response);
			return;
		}

		$sql = "SELECT * FROM accounts WHERE username = ?";
		$stmt = $DBH->prepare($sql);
		$stmt->execute(array($username));

		while ($row = $stmt->fetch()) {
			$salt = substr($row['password'], 0 , 64);
			$hash = $salt . $password;
			for ($i = 0; $i < 1000; $i++) {
				$hash = hash('sha256', $hash);
			}

			$hash = $salt.$hash;

			if ($row['username'] === $username && $hash === $row['password']) {
				$foundUser = true;
				$accountid = $row['id'];
			}
		}

	if ($foundUser) {
		$response = array (
			'success' => true,
			'id' => $accountid,
			'message' => 'Login successful'
		);

		if (!isset($_SESSION['playlist_list'])) {

			$sql = "SELECT * FROM playlists WHERE accountid = ?";
			$query = $DBH->prepare($sql);
			$query->execute(array(intval($accountid)));
	
			while ($row = $query->fetch()) {
				$_SESSION['playlist_list'][] = 
					array(
						'id' => $row['id'],
    					'name' => $row['name']
    				);
			}
			$response['playlist_list'] = $_SESSION['playlist_list'];
		}

		$_SESSION['loggedin'] = true;
		$_SESSION['id'] = $accountid;
	} else {
		$response = array(
			'success' => false,
			'message' => 'Wrong username and password combination.'
		);
	}
	} else {
		$response = array (
			'success' => false,
			'message' => 'You are already logged in.'
		);
	}

	echo json_encode($response);

?>