<?php
	include('../inc/ExtractedVideo.php');
	include("../../../gosuconfig/config.php");

	$videosToAdd = $_POST['add'];
	$videosToRemove = array();

	if (!isset($_SESSION['playlist']))
		$_SESSION['playlist'] = array();

	foreach ($videosToAdd as $video) {
		if ($_SESSION['loggedin']) {
			$sql = "INSERT INTO playlist_videos (playlistid, videoid) VALUES (?, ?)";
			$query = $DBH->prepare($sql);
			$query->execute(array($_SESSION['current_playlist'], $video['id']));
		}

		$newVideo = new ExtractedVideo($video['title'], $video['id'], $video['uploaded']);
		$newVideo->setViewCount($video['viewCount']);
		array_push($_SESSION['playlist'], $newVideo);
		array_push($videosToRemove, $newVideo);
	}
	if (!$_SESSION['loggedin']) {
		foreach ($videosToRemove as $video) {
			for ($i = 0; $i < count($_SESSION['videoList']); $i++) {
				if ($video->getId() == $_SESSION['videoList'][$i]->getId()) {
					unset($_SESSION['videoList'][$i]);
					$_SESSION['videoList'] = array_values($_SESSION['videoList']);
				}
			}
		}
	}
?>
