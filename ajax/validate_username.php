<?php
	include("../../../gosuconfig/config.php");

	$username = $_POST['username'];
	$response = array();
	if (preg_match("/^[A-Za-z0-9_]{3,20}$/", $username)) {
		$sql = "SELECT * FROM accounts WHERE username = ?";
		$query = $DBH->prepare($sql);
		$query->execute(array($username));

		if ($query->rowCount() == 0) {
			$response = array(
				'success' => true,
				'message' => "Username is valid."
			);
		} else {
			$response = array(
				'success' => false,
				'message' => "Username already exists."
			);			
		}
	} else {
		$response = array (
			'success' => false,
			'message' => 'Must be 3 to 20 characters and can only contain letters and underscores.'
		);
	}

	echo json_encode($response);
?>