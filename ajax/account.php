<?php
	include('../inc/ExtractedVideo.php');
	include("../../../gosuconfig/config.php");

	$response = array();

	if($_SESSION['loggedin']) {

		$response = array(
			'loggedin' => true
		);
		$response['current_playlist'] = $_SESSION['current_playlist'];
	    $response['playlist_list'] = $_SESSION['playlist_list'];
	    
	} else {
		$response = array(
			'loggedin' => false
		);
	}

	echo json_encode($response);
?>