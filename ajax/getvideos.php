<?php
	include('../inc/ExtractedVideo.php');
	include('../inc/VideoList.php');
    include('../inc/sortfunc.php');
	include("../../../gosuconfig/config.php");

	if (!$_SESSION['loaded']) {
		foreach ($channelsToLoad as $channel) {

			if (file_exists('../data/'.$channel->getName())) {
				$jsonFiles = scandir('../data/'.$channel->getName());
				
				foreach ($jsonFiles as $cFile) {
					if (!in_array($cFile, array('.','..'))) {
						$cFileJson = json_decode(file_get_contents('../data/'.$channel->getName().'/'.$cFile), true);
                        if (isset($cFileJson['data'])) {
                            if (array_key_exists('items', $cFileJson['data'])) {
                                foreach ($cFileJson['data']['items'] as $video) {
                                	if (preg_match($channel->getFilter(), $video['title'])) {
                                        $newVideo = new ExtractedVideo($video['title'], $video['id'], $video['uploaded']);
                                        $newVideo->setViewCount($video['viewCount']);
                                        $_SESSION['videoList'][] = $newVideo;
                                    }
                                }
                            }
                        }
					}
				}
			}
		}
        $_SESSION['sort'] = 'date';
        $_SESSION['order'] = 'd';
	}
    
    SortVideos($_SESSION['videoList'], $_SESSION['sort'], $_SESSION['order']);

	$_SESSION['loaded'] = true;
    
	$returnArray = array(
			"videoList" => $_SESSION['videoList'],
            "sortby" => $_SESSION['sort'],
            "order" => $_SESSION['order']
		);
		
	echo json_encode($returnArray);
?>