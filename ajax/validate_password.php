<?php
	include("../../../gosuconfig/config.php");
	
	$password = $_POST['password'];
	$response = array();

	if (preg_match("/^[A-Za-z0-9!@#$%^&*()_]{6,20}$/", $password)) {
		$response = array (
			'success' => true,
			'msg' => "Password is valid"
		);
	} else {
		$response = array (
			'success' => false,
			'message' => "Must be 6 to 20 characters."
		);
	}

	echo json_encode($response);
?>