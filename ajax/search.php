<?php
	include('../inc/ExtractedVideo.php');
	include('../inc/sortfunc.php');
	include("../../../gosuconfig/config.php");
	
	$searchTerms = $_POST['searchTerms'];
	$searchResults = array();
	$foundResults = false;
	$_SESSION['originalList'] = $_POST['originalList'];
	
	if (isset($searchTerms)) {
		$searchArray = explode(' ', $searchTerms);
		if ($searchTerms != '') {
			foreach ($_SESSION['videoList']as $video) {
				foreach ($searchArray as $searchTerm) {
					$newSearchTerm = strtolower($searchTerm);
					$newTitle = strtolower($video->getTitle());

					if (preg_match('/'.$newSearchTerm.'/', $newTitle)) {
						$searchResults[] = $video;
						$foundResults = true;
					}
				}
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
	
	if ($foundResults) {
		$_SESSION['videoList'] = $searchResults;
	}
	
	SortVideos($_SESSION['videoList'], $_SESSION['sort'], $_SESSION['order']);
	
	echo json_encode($_SESSION['videoList']);
	return true;
?>