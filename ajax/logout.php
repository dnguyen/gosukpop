<?php
	include("../../../gosuconfig/config.php");

	$response = array();

	if (isset($_SESSION['loggedin'])) {
		session_destroy();
		$response = array(
			'success' => true,
			'message' => 'Logged out'
		);
	} else {
		$response = array(
			'success' => false,
			'message' => 'You are not logged in.'
		);
	}

	echo json_encode($response);
?>