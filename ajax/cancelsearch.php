<?php
    include("../../../gosuconfig/config.php");
    include('../inc/sortfunc.php');
    include('../inc/ExtractedVideo.php');
    
    unset($_SESSION['videoList']);
    $_SESSION['videoList'] = array();
    
    foreach ($_SESSION['originalList'] as $video) {
        $newVideo = new ExtractedVideo($video['title'], $video['id'], $video['uploaded']);
        $newVideo->setViewCount($video['viewCount']);
        array_push($_SESSION['videoList'], $newVideo);
    }
    
    //print_r($_SESSION['videoList']);
    //$_SESSION['videoList'] = $_SESSION['originalList'];
    SortVideos($_SESSION['videoList'], $_SESSIOn['sort'], $_SESSION['order']);
    echo json_encode($_SESSION['videoList']);
    return true;
?> 