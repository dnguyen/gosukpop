<?php
	include('../inc/ExtractedVideo.php');
	include("../../../gosuconfig/config.php");

	$response = array();

	if ($_SESSION['loggedin']) {
		$playlistid = $_POST['playlist'];

		$sql = "DELETE FROM playlists WHERE id = ?";
		$query = $DBH->prepare($sql);
		$query->execute(array($playlistid));

		$sql = "DELETE FROM playlist_videos WHERE playlistid = ?";
		$query = $DBH->prepare($sql);
		$query->execute(array($playlistid));

		
		for ($i = 0; $i < count($_SESSION['playlist_list']); $i++) {
			if ($_SESSION['playlist_list'][$i]['id'] == $playlistid) {
				unset($_SESSION['playlist_list'][$i]);
				$_SESSION['playlist_list'] = array_values($_SESSION['playlist_list']);
			}
		}

		$response = array(
			'success' => true,
			'message' => 'Playlist deleted'
		);
	} else {
		$response = array (
			'success' => false,
			'message' => 'Not logged in'
		);
	}

	echo json_encode($response);
?>