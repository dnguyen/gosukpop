<?php
    include("../inc/ExtractedVideo.php");
    include("../inc/sortfunc.php");
	include("../../../gosuconfig/config.php");

	if ($_POST['s'] == 't')
	    $isSearching = true;
	else
		$isSearching = false;

    $_SESSION['sort'] = $_POST['sort'];
	$_SESSION['order'] = $_POST['order'];

    if (!$isSearching) {
	    SortVideos($_SESSION['videoList'], $_SESSION['sort'], $_SESSION['order']);
		echo json_encode($_SESSION['videoList']);
	} else {
			//print_r(json_decode($_POST['searchResults']));
			$sortedResults = array();
			$searchResults = ($_POST['searchResults']);
			foreach($searchResults as $video) {
				$newVideo = new ExtractedVideo($video['title'], $video['id'], $video['uploaded']);
				$newVideo->setViewCount($video['viewCount']);
				$sortedResults[] = $newVideo;
			}
			SortVideos($sortedResults, $_SESSION['sort'], $_SESSION['order']);
			echo json_encode($sortedResults);
	}
?>
