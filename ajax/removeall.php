<?php
	require_once('../inc/ExtractedVideo.php');
	include("../../../gosuconfig/config.php");

	for ($i = 0; $i < count($_SESSION['playlist']); $i++) {			
		if ($_SESSION['loggedin']) {
			$sql = "DELETE FROM playlist_videos WHERE playlistid = ? AND videoid = ?";
			$query = $DBH->prepare($sql);
			$query->execute(array($_SESSION['current_playlist'], $_SESSION['playlist'][$i]->getId()));
		} else {
			array_unshift($_SESSION['videoList'], $_SESSION['playlist'][$i]);
		}
	}

	$_SESSION['playlist'] = array();
?>
