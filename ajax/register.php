<?php
	include("../../../gosuconfig/config.php");

	$username = $_POST['data']['username'];
	$password = $_POST['data']['password'];
	$confirmPassword = $_POST['data']['confirmPassword'];

	$response = array();

	if (preg_match("/^[A-Za-z0-9_]{3,20}$/", $username)) {
		if (preg_match("/^[A-Za-z0-9!@#$%^&*()_]{6,20}$/", $password)) {
			if ($password === $password) {
				$sql = "SELECT * FROM accounts WHERE username = ?";
				$query = $DBH->prepare($sql);
				$query->execute(array($username));

				if ($query->rowCount() == 0) {
					
					$salt = hash('sha256', uniqid(mt_rand(), true));
					$hash = $salt.$password;
					for ($i = 0; $i < 1000; $i++) {
						$hash = hash('sha256', $hash);
					}

					$hash = $salt.$hash;

					$sql = "INSERT INTO accounts(username, password) VALUES (:username, :password)";
					$query = $DBH->prepare($sql);
					$exec = $query->execute(
						array(
							':username' => $username,
							':password' => $hash
						));

					if ($exec) {
						$response = array (
							'success' => true,
							'message' => 'Registration complete.',
							'pw' => $hash
						);
					}else {
						$response = array (
							'success' => false,
							'message' => 'Error with registration.'
						);
					}
				} else {
					$response = array (
						'success' => false,
						'message' => 'Username already exists.'
					);
				}
			} else {
				$response = array (
					'success' => false,
					'message' => 'Passwords do not match.'
				);
			}
		} else {
			$response = array(
				'success' => false,
				'message' => 'Invalid password.'
			);
		}
	} else {
		$response = array(
			'success' => false,
			'message' => 'Must be 3 to 20 characters and can only contain letters and underscores.'
		);
	}

	echo json_encode($response);
?>