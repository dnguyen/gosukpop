<?php
	include("../../../gosuconfig/config.php");

	$password = $_POST['password'];
	$confirmPassword = $_POST['confirm'];

	$response = array();

	if ($password === $confirmPassword) {
		$response = array (
			'success' => true,
			'message' => "Password confirmed."
		);
	} else {
		$response = array (
			'success' => false,
			'message' => "Passwords do not match."
		);
	}

	echo json_encode($response);
?>