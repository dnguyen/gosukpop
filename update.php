<?php	

	include("inc/VideoList.php");

	$lastUpdatedFileName = 'data/lastupdated.txt';
	$lastUpdatedFile = fopen($lastUpdatedFileName, 'r');
	$lastUpdatedTime = fread($lastUpdatedFile, filesize($lastUpdatedFileName));
	fclose($lastUpdatedFile);

	foreach ($channelsToLoad as $channel) {
		$url = fopen('http://gdata.youtube.com/feeds/api/users/'.$channel->getName().'?alt=json', 'r');
		$channelJson = json_decode(stream_get_contents($url), true);
		$lastChannelUpdate = $channelJson['entry']['updated']['$t'];
		$lastChannelUpdate = explode("T", $lastChannelUpdate);

		// If the folder does not exist yet
		// Then create the folder and get data.
		if (!file_exists('data/'.$channel->getName())) {
			echo $channel->getName().' did not exist. Created data.</br>';
			$totalVideosFile = fopen('http://gdata.youtube.com/feeds/api/users/'.$channel->getName().'/uploads?v=2&alt=jsonc', 'r');
			$totalVideosJson = json_decode(stream_get_contents($totalVideosFile), true);
			$totalVideos = intval($totalVideosJson['data']['totalItems']);

			for ($i = 1; $i < $totalVideos; $i+=9) {
				$channelVideos = fopen('http://gdata.youtube.com/feeds/api/users/'.$channel->getName().'/uploads?v=2&alt=jsonc&orderby=published&max-results=10&start-index='.$i, 'r');

				if (!file_exists('data/'.$channel->getName())) {
					mkdir('data/'.$channel->getName(), 0755);
				}

				$newChannelFile = fopen('data/'.$channel->getName().'/'.$channel->getName().$i.'.js', 'w');
				fwrite($newChannelFile, stream_get_contents($channelVideos));
				fclose($newChannelFile);

			}
		}

		/*
			Video list needs to updated.
			Download JSON files
		*/
		if (intval(strtotime($lastChannelUpdate[0])) > intval($lastUpdatedTime)) {
			echo 'Update for '.$channel->getName().' needed';

			$totalVideosFile = fopen('http://gdata.youtube.com/feeds/api/users/'.$channel->getName().'/uploads?v=2&alt=jsonc', 'r');
			$totalVideosJson = json_decode(stream_get_contents($totalVideosFile), true);
			$totalVideos = intval($totalVideosJson['data']['totalItems']);

			for ($i = 1; $i < $totalVideos; $i+=9) {
				$channelVideos = fopen('http://gdata.youtube.com/feeds/api/users/'.$channel->getName().'/uploads?v=2&alt=jsonc&orderby=published&max-results=10&start-index='.$i, 'r');

				if (!file_exists('data/'.$channel->getName())) {
					mkdir('data/'.$channel->getName(), 0755);
				}

				$newChannelFile = fopen('data/'.$channel->getName().'/'.$channel->getName().$i.'.js', 'w');
				fwrite($newChannelFile, stream_get_contents($channelVideos));
				fclose($newChannelFile);

			}

			$lastUpdatedFile = fopen($lastUpdatedFileName, 'w');
			fwrite($lastUpdatedFile, date("U"));
			fclose($lastUpdatedFile);
			
			echo '........Done</br>';
		} else {
			echo 'Update to '.$channel->getName().' not needed</br>';
		}
	}
	
?>