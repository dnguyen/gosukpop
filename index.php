<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title>gosukpop - Listen to your favorite KPop music videos</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
        <meta name="description" content="Create KPop music video playlists.">
        <meta name="keywords" content="kpop, music videos">

		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/application.css">
		<link rel="stylesheet" href="css/bootstrap.responsive.css">
		<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
		<!--<script src="js/less-1.2.1.min.js"></script>-->

		<!--<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-27935913-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>-->

	</head>
	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div id="header">
					<div id="logo"><a href="index.php"><img src="images/logo.png"/></a></div>
					<form id="login-form" class="navbar-form" onsubmit="return false;">
						<input id="username" class="span2" type="text" placeholder="Username" name="username" />
						<input id="password" class="span2" type="password" placeholder="Password" name="password" />
						<button id="loginButton" class="btn small primary custom">Log In</button>
						<button id="registerButton" class="btn small">Register</button>
					</form>
				</div>
			</div>
		</div>

		<div id="container" class="container">
			<div class="content">
				<div id="playlist-container" class="row">
					<div id="playlist-wrapper" class="span6">
						<div id="playlist-header">
							<h3>Playlist</h3>
							<div id="playlist-controls-container">

								<ul id="playlist-controls">
									<li><a class="prev" title="Previous video"><img src="images/prev.png"/></a></li>
									<li><a class="next" title="Next video"><img src="images/next.png"/></a></li>
									<li><button id="removeAll" class="btn small btn-danger">Remove All</button></li>
								</ul>
							</div>
						</div>
						<div class="clear"></div>
						<div id="playlist-table">
							<table id="playlist" class="table table-striped table-condensed">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div id="player" class="span6"></div>
				</div>
				<div class="content">
					<div id="main" class="row">
						<div id="mainListTable-container" class="span12">
							<h3>Music Videos</h3>
							<ul id="mainListOptions">
								<li><button id="addAllButton" class="btn primary custom">Add all</button></li>
								<li>
									<select id="sort-options" class="span2">
										<option value="date">Sort by Date</option>
										<option value="views">Sort by Views</option>
									</select>
								</li>
								<li>
									<select id="order" class="span2">
										<option value="a">Ascending</option>
										<option value="d">Descending</option>
									</select>
								</li>
								<li><button id="refresh-videos" class="btn primary custom" title="Update video list">Update Videos</button></li>
							</ul>
							<form id="search-form" class="form-search" method="post" onsubmit="return false;">
								<div id="search">
									<div id="search-box">
										<input id="search-input" type="text"  name="s" placeholder="Search" />
									</div>
									<button id="search-button" type="submit" class="btn primary custom">Search</button>
								</div>
							</form>
							<div class="clear"></div>
							<div id="loading" class="alert alert-info">
								<p>Loading videos...please wait.</p>
							</div>
							<div id="mainListTable-wrapper">
							<table id="mainListTable" class="table table-striped">
								<tbody>
								</tbody>
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="push"></div>
		</div>

		<div id="footer">
			<div id="footer-content" class="content">
				<a href="index.php"><img src="images/footer-logo.png" alt="gosukpop"/></a>
				<div id="social">
					<h5>social</h5>
					<ul id="social-list">
						<li><a href="http://reddit.com/r/kpop" title="Reddit" target="_blank"><img src="images/reddit.png"/></a></li>
						<li><a href="http://twitter.com/dylanguyen" title="Twitter" target="_blank"><img src="images/twitter.png"/></a></li>
					</ul>
				</div>
			</div>
		</div>

		<div id="register-modal" class="modal hide fade">
			<div class="modal-header">
				<a class="close" data-dismiss="modal">×</a>
				<h3>Register</h3>
			</div>
			<div id="register-body" class="modal-body">
				<form id="register-form" onsubmit="return false;">
					<fieldset>
						<label for="username-register"><h4>Username</h4></label>
						<div class="input">
							<input type="text" id="username-register" name="username"/>
						</div>
					</fieldset>
						<div class="clearfix"></div>
					<fieldset>
						<label for="password-register"><h4>Password</h4></label>
						<div class="input">
							<input type="password" id="password-register" name="password"/>
						</div>
					</fieldset>
						<div class="clearfix"></div>
					<fieldset>
						<label for="confirm-password-register"><h4>Confirm Password</h4></label>
						<div class="input">
							<input type="password" id="confirm-password-register" name="confirmPassword"/>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="modal-footer">
				<a title="Register" id ="register-button" class="btn primary custom">Register</a>
			</div>
		</div>

		<script id="mainVideoItem-template" type="text/template">
			<td class="video" id="<%= id %>">
				<ul id="video-options">
					<li><span class="add"><a title="Add to playlist"><img src="images/add.png"/></a></span></li>
					<li><span class="expand"><a title="Preview"><img src="images/expand.png"/></a></span></li>
				</ul>
				<p class="title"><%= title %></p>
				<div class="clear"></div>
				<div class="videoPreview"></div>
			</td>
			</script>

		<script id="playlist-template" type="text/template">
			<td id="<%= id %>">
				<ul class="playlist-options">
					<li><a class="play" title="Play"><img src="images/play.png"/></a></li>
					<li><a class="remove" title="Remove"><img src="images/remove.png"/></a></li>
				</ul>
				<span class="title"><p><%= title %></p></span>
			</td>
		</script>

		<script id="playlists-list-template" type="text/template">
			<select id="savedplaylists" class="span2">
				<option value="0">Select playlist</option>
			</select>
			<a id="new-playlist" title="New playlist"><img src="images/new.png"/></a>
			<a id="delete-playlist" title="Delete playlist"><img src="images/delete-playlist.png"/></a>
		</script>

		<script id="new-playlist-template" type="text/template">
			<div id="new-playlist-modal" class="modal hide fade">
				<div class="modal-header">
					<a class="close" data-dismiss="modal">×</a>
					<h3>Create a new playlist</h3>
				</div>
				<div class="modal-body">
					<p>Enter a name for your playlist</p>
					<fieldset>
						<label for="new-playlist-name">Name</label>
						<input type="text" id="new-playlist-name" class="span5"></input>
					</fieldset>
				</div>
				<div class="modal-footer">
					<a title="Create" id="new-playlist-button" class="btn primary">Create</a>
				</div>
			</div>			
		</script>

		<script id="alert-template" type="text/template">
			<div id="alert-message" class="alert alert-<%= type %>">
				<a class="close" data-dismiss="alert"><i class="icon-remove"></i></a>
				<h4 class="alert-heading"><%= message %></h4>
			</div>
		</script>

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script src="js/underscore-min.js"></script>
        <script src="js/backbone.js"></script>
		<script src="js/ytplayer.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-tooltip.js"></script>
		<script src="js/bootstrap-alert.js"></script>

        <script src="app/views/LoginView.js"></script>
        <script src="app/models/RegisterModel.js"></script>
        <script src="app/views/RegisterView.js"></script>
        <script src="app/models/VideoPlayer.js"></script>
        <script src="app/models/Video.js"></script>
        <script src="app/collections/VideoCollection.js"></script>
        <script src="app/models/MainVideoList.js"></script>
        <script src="app/views/MainVideoItemView.js"></script>
        <script src="app/views/MainVideoListView.js"></script>
        
        <script src="app/models/Playlist.js"></script>
        <script src="app/views/PlaylistVideoItemView.js"></script>
        <script src="app/views/PlaylistView.js"></script>
        <script src="app/views/NewPlaylistView.js"></script>

        <script src="app/models/AlertModel.js"></script>
        <script src="app/views/AlertView.js"></script>

        <script src="app/routers/AppRouter.js"></script>
        
        <script src="app/application.js"></script>

	</body>
</html>
