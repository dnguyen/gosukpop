<?php
    function ViewsQuickSort($array, $order) {
        if (count($array) <= 1)
            return $array;
        
        $pivot = $array[count($array) / 2];
        unset($array[count($array) / 2]);
        $array = array_values($array);
        
        $less = array();
        $greater = array();
        
        foreach ($array as $x) {
            if ($order == 'a') {
                if (intval($x->getViewCount()) <= intval($pivot->getViewCount()))
                    $less[] = $x;
                else
                    $greater[] = $x;
            } else {
                if (intval($x->getViewCount()) >= intval($pivot->getViewCount()))
                    $greater[] = $x;
                else
                    $less[] = $x;                
            }
        }
        if ($order == 'a')
            return array_merge(ViewsQuickSort($less, $order), array($pivot), ViewsQuickSort($greater, $order));
        else
            return array_merge(ViewsQuickSort($greater, $order), array($pivot), ViewsQuickSort($less, $order));      
    }
    
    function DateQuickSort($array, $order) {
        if (count($array) <= 1)
            return $array;
        
        $pivot = $array[count($array) / 2];
        unset($array[count($array) / 2]);
        $array = array_values($array);
        
        $less = array();
        $greater = array();
        
        foreach ($array as $x) {
            if ($order == 'a') {
                if ($x->getUploadedDate() <= $pivot->getUploadedDate())
                    $less[] = $x;
                else
                    $greater[] = $x;
            } else {
                if ($x->getUploadedDate() >= $pivot->getUploadedDate())
                    $greater[] = $x;
                else
                    $less[] = $x;                
            }
        }
        if ($order == 'a')
            return array_merge(DateQuickSort($less, $order), array($pivot), DateQuickSort($greater, $order));
        else
            return array_merge(DateQuickSort($greater, $order), array($pivot), DateQuickSort($less, $order));           
    }
    
    function SortVideos(&$array, $type, $order) {
        if ($type == 'views') {
            $array = ViewsQuickSort($array, $order);
        } else if ($type == 'date') {
            $array = DateQuickSort($array, $order);
        }
    }
?>