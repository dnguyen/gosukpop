<?php
	class Channel {
		private $name;
		private $filterPattern;

		public function __construct($name_, $filter) {
			$this->name = $name_;
			$this->filterPattern = $filter;
		}

		public function getName() {
			return $this->name;
		}

		public function getFilter() {
			return $this->filterPattern;
		}
	}
?>