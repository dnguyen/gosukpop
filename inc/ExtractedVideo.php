<?php
	class ExtractedVideo {
		public $title;
		public $id;
		public $uploaded;
		public $viewCount;
		
		public function __construct($title_, $id_, $uploaded_) {
			$this->title = $title_;
			$this->id = $id_;
			$this->uploaded = $uploaded_;
		}

		public function getTitle() {
			return $this->title;
		}

		public function getId() {
			return $this->id;
		}
		
		public function getUploadedDate() {
			$uploaddate = explode("T", $this->uploaded);
			return strtotime($uploaddate[0]);
		}
		
		public function getViewCount() {
			return $this->viewCount;
		}
		
		public function setViewCount($views) {
			$this->viewCount = $views;
		}
	}
?>