var VideoPlayer = Backbone.Model.extend({
   player: null,
   load: function(videoid) {
    	this.player = new YT.Player('player', {
		  height: '284',
		  width: '460',
		  videoId: videoid,
		  events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		  }
		});
   }
});
