var VideoCollection = Backbone.Collection.extend({
    
    model: Video,
    
    /*comparator: function(video) {
        if (mainVideoList.get('sort') == "date") {
            if (mainVideoList.get('order') == "a") {
                return -video.get("uploaded");
            } else {
                return video.get("uploaded");
            }
        } else {
            if (mainVideoList.get('order') == "a") {
                return -video.get('views');
            } else {
                return video.get('views');
            }
        }
    },*/
    
    rebuild: function(list, prepend_) {
        this.reset();
        for (var i = 0; i < list.length; i++) {
            this.add(new Video({
                title: list[i].title,
                id: list[i].id,
                uploaded: list[i].uploaded,
                viewCount: list[i].viewCount,
                prepend: prepend_
            }));
        }
    },
    
    search: function(terms) {
        var pattern = new RegExp(terms.toLowerCase());
        return _(this.filter(function(data){
            return pattern.test(data.get("title").toLowerCase());
        }));
    }
    
});