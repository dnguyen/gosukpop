$(document).ready(function() {
	var loggedin = false;
	window.loginView = new LoginView();
	window.registerView = new RegisterView({ model : new RegisterModel() });
	window.mainVideoList = new MainVideoList();
    window.playlist = new PlaylistModel();
    window.searchResults;
	window.mainVideoListView = new MainVideoListView();
    window.playlistView = new PlaylistView();
	
	var appRouter = new AppRouter;
	Backbone.history.start();
});
