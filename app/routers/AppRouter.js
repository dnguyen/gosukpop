var AppRouter = Backbone.Router.extend({
    routes: {
    	"" : "home",
        "account" : "search"
    },

    home : function() {
        $.ajax({
            type: "GET",
            url: 'ajax/account.php',
            dataType: 'json',
            success: function(output) {
                if (output['loggedin']) {
                    $('#login-form').hide();
                    $('#logo').after('<div id="account"><p><a id="account-link">My account</a> | <a id="logout">Log out</a></p></div>');
                    var playlist_controls = $('#playlist-controls-container');
                    playlist_controls.prepend(_.template($('#playlists-list-template').html()));
                    if (output['playlist_list'] != null) {
                        var playlistHtml = "";
                        for (var i = 0; i < output['playlist_list'].length; i++) {
                            playlistHtml += "<option value="+output['playlist_list'][i]['id']+">"+output['playlist_list'][i]['name']+"</option>";
                        }

                        $('#savedplaylists').append(playlistHtml);
                    }
                    if (output['current_playlist'] != null) {
                        $('#savedplaylists').val(output['current_playlist']);
                    }
                    $('#playlist-controls').css({'float' : 'right'});
                    loggedin = true;
                } else {
                    loggedin = false;
                    $('#account').remove();
                    $('#login-form').show();
                    $('#playlist-controls').css({'float' : 'left', 'margin-bottom' : '10px'});
                }
            },
            error: function() {
            }
        });	
    },

    search: function() {
        alert("Account");
    }
});