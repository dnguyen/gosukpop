var NewPlaylistView = Backbone.View.extend({
	template: _.template($('#new-playlist-template').html()),
	events: {
		"click #new-playlist-button" : "create"
	},

	show : function() {
		$('#footer').after(this.template);
		this.el = $('#new-playlist-modal');
		$(this.el).modal(
			{ backdrop: false },
			'show'
		).css({
			'width' : '410px',
			'margin-left' : '-205px'
		});
		this.delegateEvents(this.events);
	},

	create : function() {
		var this_ = this;
		$.ajax({
			type: "POST",
			url: "ajax/newplaylist.php",
			dataType: "json",
			data: "name="+$('#new-playlist-name').val(),
			success: function(output) {
				if (output['success']) {
					$('#savedplaylists').append('<option value='+output['id']+'>'+output['name']+'</option');
					$('#savedplaylists').tooltip({
						title : "Added a new playlist!",
						trigger: 'manual'
					});
					$('#savedplaylists').tooltip('show');
					this_.remove();
				} else {
					$('#new-playlist-modal .modal-body').find('.new-playlist-error').remove();
					$('#new-playlist-modal .modal-body').append('<p class="new-playlist-error">'+output['message']+'</p>');
				}
			}, error : function() {
				
			}

		});
	}

});