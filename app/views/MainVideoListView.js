var MainVideoListView = Backbone.View.extend({
    
    el: $('#main'),
    
    events: {
        "click #search-button" : "search",
        "change #search-box[type='text']" : "searchChanged",
        "change #sort-options" : "sortChanged",
        "change #order" : "sortChanged",
        "click #cancelSearch" : "cancelSearchClicked",
        "click #addAllButton" : "addAllClicked",
        "click #refresh-videos" : "refreshVideosClicked"
	},
    
    initialize: function() {
        mainVideoList.videoCollection.bind('add', this.addVideo);
        mainVideoList.videoCollection.bind('remove', this.removeVideo);
         
        this.loadVideos();
    },
   
    loadVideos: function() {
        $.ajax({
            type: "POST",
            url: "ajax/getvideos.php",
            dataType: 'json',
            success: function(output) {
                $('#sort-options').val(output['sortby']);
                $('#order').val(output['order']);                
                
                var selectedSort = $("#sort-options").val();
                var selectedOrder = $("#order").val();
                
                mainVideoList.videoCollection.rebuild(output['videoList'], false);
                
                $('#loading').hide();
            },
            error: function() {
                alert("Error with loading videos.");
            }
        });
    },
    
    addVideo: function(video) {
        var mainVideoItem = new MainVideoItemView({model: video});
        if (video.get('prepend')) {
            $('#mainListTable tbody').prepend(mainVideoItem.render().el);
        } else {
            $('#mainListTable tbody').append(mainVideoItem.render().el);
        }
    },
    
    removeVideo: function(video) {
        
    },
    
    search: function(e) {
		var searchTerms = $('input#search-input').val();
		var searchLink = $("#search-link").attr('href');
        $("#search-link").attr("href", searchLink+searchTerms);
        
		if (searchTerms.length > 0) {
			mainVideoList.set( {searching: true} );
            $('#mainListTable tbody tr').each(function() {
                $(this).remove();
            });
			searchResults = mainVideoList.videoCollection.search(searchTerms);
            _.each(searchResults._wrapped, function(newvideo) {
                var mainVideoItem = new MainVideoItemView({model: newvideo});
                $('#mainListTable tbody').append(mainVideoItem.render().el);
            });
            
			$('#addAllButton').show();

			$('#search-box').removeClass('input-append');
            $('#cancelSearch').remove();
            
            $('#search-box').attr('class', 'input-append');
            $('#search-input').after('<span id="cancelSearch" class="add-on"><i id="remove-icon" class="icon-remove"></i></span>');
		}
    },
    
    searchChanged: function(e) {
        if ($(e.currentTarget).val() == '') {
            $('#mainListTable tbody tr').each(function() {
                $(this).remove();
            });
            $('#loading').show();
            this.loadVideos();
            $('#search-box').removeClass('input-append');
            $('#cancelSearch').remove();

			$('#addAllButton').hide();
			mainVideoList.set({ searching: false });
        }
    },
    
    cancelSearchClicked: function(e) {
        $('#mainListTable tbody tr').each(function() {
            $(this).remove();
        });
        
        $('#search-input').val('');
        $('#search-box').removeClass('input-append');
        $('#cancelSearch').remove();

        $('#loading').show();
        this.loadVideos();
        
        $(e.currentTarget).hide();
		$('#addAllButton').hide();
		mainVideoList.set({ searching: false });
    },
    
    sortChanged: function() {
        var selectedSort = $("#sort-options").val();
		var selectedOrder = $("#order").val();

		if (!mainVideoList.get('searching')) {
			$.ajax({
				type: "POST",
				url: "ajax/sort.php",
				dataType: "json",
				data: "s=f&sort="+selectedSort+"&order="+selectedOrder,
				success: function(output) {
					$('#mainListTable tbody tr').each(function() {
                    	$(this).remove();
	                });
                
        	        mainVideoList.videoCollection.rebuild(output, false);
				},
				error: function() {
					alert("Error with sorting videos");
				}
			});	
		} else {
			var searchArray = new Array();
            _.each(searchResults._wrapped, function(newvideo) {
                searchArray.push({
						'id': newvideo.attributes.id,
						'title': newvideo.attributes.title,
						'uploaded': newvideo.attributes.uploaded,
						'viewCount': newvideo.attributes.viewCount
						
				});
            });
			$.ajax({
				type: "POST",
				url: "ajax/sort.php",
				dataType: "json",
				data: 			
					{
						's': 't',
						'sort': selectedSort,
						'order': selectedOrder,
						'searchResults':searchArray
					},
				success: function(output) {
					$('#mainListTable tbody tr').each(function() {
                   		$(this).remove();
	                });

					for (var i = 0; i < output.length; i++) {
						var newvideo = new Video({
							'title': output[i].title,
							'id': output[i].id,
							'uploaded': output[i].uploaded,
							'viewCount': output[i].viewCount
						});
               			var mainVideoItem = new MainVideoItemView({model: newvideo});
                		$('#mainListTable tbody').append(mainVideoItem.render().el);
					}
				},
				error: function() {
					alert("Error");
				}
			});	
		}
    },
    
    addAllClicked: function() {
		if (mainVideoList.get('searching')) {
			var addArray = new Array();
            _.each(searchResults._wrapped, function(newvideo) {
                addArray.push({
						'id': newvideo.attributes.id,
						'title': newvideo.attributes.title,
						'uploaded': newvideo.attributes.uploaded,
						'viewCount': newvideo.attributes.viewCount
				});
            });

			$.ajax({
				type: "POST",
				url: "ajax/addall.php",
				dataType: "json",
				data: 			
					{
						'add':addArray
					},
				success: function() {
					if (!loggedin) {
						$('#mainListTable tbody tr').each(function() {
							$(this).remove();
						});
					}

					var domid = playlist.playlistCollection.models.length;
					_.each(searchResults._wrapped, function(newvideo) {
						mainVideoList.videoCollection.remove(searchResults._wrapped);
						newvideo.set({ domid : domid });
						newvideo.set({ prepend: true });
						playlist.set({currentVideo: 0}, { silent: true });
						playlist.playlistCollection.add(newvideo, { silent: true });
						var playlistRow = new PlaylistVideoItemView({model: newvideo});
						$('#playlist tbody').append(playlistRow.render().el);
						domid++;
					});
				}
			});
		}
    },

    refreshVideosClicked : function() {
    	  	
    }
});
