var RegisterView = Backbone.View.extend({
	el: $('#register-modal'),
	events: {
		"click #register-button" : "register",
		"blur #username-register" : "validateUsername",
		"blur #password-register" : "validatePassword",
		"blur #confirm-password-register" : "validateConfirmPassword"
	},

	initialize: function() {
		this.model.set({ 'errors' : true });
	},

	show: function() {
		$(this.el).modal(
			{ backdrop: true },
			'show'
		);
	},

	register: function() {
		var usernameInput = $('#username-register').val();
		var passwordInput = $('#password-register').val();
		var confirmPasswordInput = $('#confirm-password-register').val();

		if (!this.model.get('errors')) {
			var registrationData = {
				username : usernameInput,
				password: passwordInput,
				confirmPassword: confirmPasswordInput
			};

			$.ajax({
	            type: "POST",
	            url: "ajax/register.php",
	            data: 
	           		{
	           			'data' : registrationData
		        	},
	            dataType: 'json',
	            success: function(output) {
                    $('#container .content').find('#alert-message').remove();
	            	if (output['success']) {
	            		$('#register-modal').modal('hide');
	            		var modelAlert = new AlertModel({ type : 'success', message : 'Registration was successfull! You may now log in.' });
	            		var alertview = new AlertView({model: modelAlert}); 
	            		alertview.show();
	            	} else {
	            		$('#register-modal').modal('hide');
	            		var modelAlert = new AlertModel({ type : 'error', message : 'Something went wrong with the registration. Please try again.' });
	            		var alertview = new AlertView({model: modelAlert}); 
	            		alertview.show();	            		
	            	}
	            },
	            error: function() {
	            	alert("Error with registration.");
	            }
			});
		} else {
		}
	},

	validateUsername: function() {
		var usernameInput = $('#username-register').val();
		var tempThis = this;
		$.ajax({
            type: "POST",
            url: "ajax/validate_username.php",
            data: "username="+usernameInput,
            dataType: 'json',
            success: function(output) {
            	if (output['success']) {
					$('#username-register').parent().parent().attr('class', 'control-group success');
					$('#username-register').parent().find('.help-inline').remove();
					tempThis.model.set({ 'errors' : false });
            	} else {
					$('#username-register').parent().parent().attr('class', 'control-group error');
					if ($('#username-register').parent().parent().find('.help-inline').length == 0) {
						$('#username-register').after('<span class=help-inline>'+output['message']+'</span>');
					}	
					tempThis.model.set({ 'errors' : true });
            	}
            },
            error: function() {
            	alert("Error with registration.");
            }
		});
	},

	validatePassword: function() {
		var password = $('#password-register').val();
		var tempThis = this;
		$.ajax({
            type: "POST",
            url: "ajax/validate_password.php",
            data: "password="+password,
            dataType: 'json',
            success: function(output) {
            	if (output['success']) {
            		$('#password-register').parent().parent().attr('class', 'control-group success');
					$('#password-register').parent().find('.help-inline').remove();
					tempThis.model.set({ 'errors' : false });
            	} else {
					$('#password-register').parent().parent().attr('class', 'control-group error');
					if ($('#password-register').parent().parent().find('.help-inline').length == 0) {
						$('#password-register').after('<span class=help-inline>'+output['message']+'</span>');
					}           		
					tempThis.model.set({ 'errors' : true });
            	}
            },
            error: function() {
            	alert("Error with registration.");
            }
		});
	},

	validateConfirmPassword : function() {
		var tempThis = this;
		var password = $('#password-register').val();
		var confirmPass = $('#confirm-password-register').val();
		$.ajax({
            type: "POST",
            url: "ajax/validate_confirm_password.php",
            data: "password="+password+"&confirm="+confirmPass,
            dataType: 'json',
            success: function(output) {
            	if (output['success']) {
					$('#confirm-password-register').parent().parent().attr('class', 'control-group success');
					$('#confirm-password-register').parent().find('.help-inline').remove();
					tempThis.model.set({ 'errors' : false });
            	} else {
					$('#confirm-password-register').parent().parent().attr('class', 'control-group error');
					if ($('#confirm-password-register').parent().parent().find('.help-inline').length == 0) {
						$('#confirm-password-register').after('<span class=help-inline>'+output['message']+'</span>');
					}		          
					tempThis.model.set({ 'errors' : true });  		
            	}
            },
            error: function() {
            	alert("Error with registration.");
            }
		});
	}
});