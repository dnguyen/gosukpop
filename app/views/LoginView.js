var LoginView = Backbone.View.extend({
    el: $('#header'),
    events: {
        "click #loginButton": "login",
        "click #registerButton": "register",
        "click #logout": "logout"

    },

    login: function () {
        var loginData = {
            'username': $('#username').val(),
            'password': $('#password').val()

        };
        $.ajax({
            type: "POST",
            url: 'ajax/login.php',
            data: {
                'data': loginData
            },
            dataType: 'json',
            success: function (output) {
                if (output['success']) {
                    loggedin = true;
                    $('#login-form').hide();
                    $('#logo').after('<div id="account"><p><a id="account-link">My account</a> | <a id="logout">Log out</a></p></div>');
                    var playlist_controls = $('#playlist-controls-container');
                    playlist_controls.prepend(_.template($('#playlists-list-template').html()));

                    if (output['playlist_list'] != null) {
                        var playlistHtml = "";
                        for (var i = 0; i < output['playlist_list'].length; i++) {
                            playlistHtml += "<option value=" + output['playlist_list'][i]['id'] + ">" + output['playlist_list'][i]['name'] + "</option>";
                        }
                        $('#savedplaylists').append(playlistHtml);
                    }
                    $('#container .content').find('#alert-message').remove();
                    $('#playlist-controls').css({
                        'float': 'right'
                    });
                } else {
                    $('#container .content').find('#alert-message').remove();
                    var modelAlert = new AlertModel({
                        type: 'error',
                        message: output['message']
                    });
                    var alertview = new AlertView({
                        model: modelAlert
                    });
                    alertview.show();
                }
            },
            error: function () {
                alert("Error with registration.");
            }
        });
    },

    register: function () {
        registerView.show();
    },

    logout: function () {
        $.ajax({
            type: "GET",
            url: 'ajax/logout.php',
            dataType: 'json',
            success: function (output) {
                if (output['success']) {
                    loggedin = false;

                    $('#playlist tbody tr').each(function () {
                        $(this).remove();
                    });
                    $('#player').remove();
                    $('#savedplaylists').remove();
                    $('#new-playlist').remove();
                    $('#delete-playlist').remove();
                    $('#account').remove();
                    $('#login-form').show();
                    $('#playlist-controls').css({
                        'float': 'left',
                        'margin-bottom': '10px'
                    });
                } else {

                }
            },
            error: function () {
                alert("Error with registration.");
            }
        });
    }
});