var AlertView = Backbone.View.extend({
	template: _.template($('#alert-template').html()),

	show : function() {
		$(this.el).html(this.template(this.model.toJSON()));
		$('#playlist-container').before(this.el);
	}
});