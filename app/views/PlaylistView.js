var PlaylistView = Backbone.View.extend({
    el: $('#playlist-container'),
    
	events: {
       	"click #playlist-controls a.next" : "playNext",
       	"click #playlist-controls a.prev" : "playPrev",
		"click #playlist-controls #removeAll" : "removeAll",
		"change #savedplaylists" : "playlistChanged",
		"blur #savedplaylists" : "playlistsBlur",
		"click #new-playlist" : "newPlaylistClicked",
		"click #delete-playlist" : "deletePlaylistClicked"
	},
	
    initialize: function() {
		if (playlist.playlistCollection == null)
			playlist.playlistCollection = new VideoCollection();
			
        playlist.playlistCollection.bind('add', this.addVideo);
        playlist.playlistCollection.bind('remove', this.removeVideo);
		playlist.bind('change:currentVideo', this.changeVideo);
		
        this.load();
    },
    
    load: function() {
        $.ajax({
            type: "GET",
            url: "ajax/playlist.php",
            dataType: 'json',
            success: function(output) {
                for (var i = 0; i < output['playlist'].length; i++) {
                    playlist.playlistCollection.add(new Video({
                        title: output['playlist'][i].title,
                        id: output['playlist'][i].id,
                        uploaded: output['playlist'][i].uploaded,
                        viewCount: output['playlist'][i].viewCount,
						domid: i
                    }), { silent: true });
					
					var playlistRow = new PlaylistVideoItemView({model: playlist.playlistCollection.models[i]});
					$('#playlist tbody').append(playlistRow.render().el);
                }
            },
            error: function() {
                alert("Error with getting playlist");
            }
        });
    },
    
	changeVideo: function() {
        $('#playlist-container').find('#player').remove();
        $('#playlist-wrapper').after('<div id=player class=span6></div>');
		window.videoPlayer = new VideoPlayer();
        window.videoPlayer.load(playlist.playlistCollection.models[playlist.get('currentVideo')].attributes.id);
		$('#playlist tr#' + playlist.get('currentVideo')).css('background', '#DAFFC2');
	},

    playNext : function() {
        if (playlist.playlistCollection.models.length > 1) {
			var newVideo = playlist.get('currentVideo');
			$('#playlist tr#' + newVideo).css('background',  '#FFF');
			if (playlist.get('currentVideo') >= playlist.playlistCollection.models.length - 1) {
				playlist.set({ currentVideo: 0 });
			} else {
				playlist.set({ currentVideo: parseInt(playlist.get('currentVideo')) + 1 });
			}
		}
    },
    
    playPrev : function() {
		if (playlist.playlistCollection.models.length > 1) {
			var newVideo = playlist.get('currentVideo');
			$('#playlist tr#' + newVideo).css('background',  '#FFF');
			if (playlist.get('currentVideo') <= 0) {
				playlist.set({ currentVideo: playlist.playlistCollection.models.length - 1 });
			} else {
				playlist.set({ currentVideo: parseInt(playlist.get('currentVideo')) - 1});
			}
		}
    },
	
    addVideo: function(video) {
		$.ajax({
			type: "POST",
			url: "ajax/add.php",
			dateType: "text",
			data: "v="+video.id,
			success: function(output) {
				video.set({prepend:true});
				var playlistRow = new PlaylistVideoItemView({model: video});
				$('#playlist tbody').append(playlistRow.render().el);
			},
			error: function() {
				alert("Error with adding song to playlist.");
			}
		}); 
    },
	
	removeVideo: function(video) {
		$.ajax({
			type: "POST",
			url: "ajax/remove.php",
			dateType: "text",
			data: "v="+video.get('id'),
			success: function(output) {
				var selectedid = 0;
				$('#playlist tr').each(function() {
					if (parseInt(this.id) > parseInt(video.get('domid'))) {
						selectedid = this.id;
						this.id = parseInt(this.id) - 1;
					}
				});
				
				if (playlist.get('currentVideo') == video.get('domid')) {
					if (playlist.playlistCollection.length > 1) {
						playlist.trigger('change:currentVideo');
					} else {
						$('#player').remove();
					}
				}
				if (!loggedin) {
					video.set({prepend:true});
					mainVideoList.videoCollection.add(video);
				}
			},
			error: function() {
				alert("Error with removing a song from the playlist.")
			}
		});
	},

	removeAll: function(e) {
		if (playlist.playlistCollection.models.length > 0) {
			$.ajax({
				type: "POST",
				url: "ajax/removeall.php",
				dateType: "json",
				success: function(output) {
					$('#playlist tbody tr').each(function() {
						$(this).remove();
					});
					$('#player').remove();
					
					playlist.set({currentVideo: 0}, { silent: true });
					mainVideoList.videoCollection.add(playlist.playlistCollection.models);
					playlist.playlistCollection.reset();

				},
				error: function() {
					alert("Error with removing all videos");
				}
			});
		}
	},

	playlistChanged : function() {
		if ($('#savedplaylists').val() != '0') {
			var playlistid = +$('#savedplaylists').val();
			$.ajax({
				type: "GET",
				url: "ajax/playlist.php",
				dateType: 'json',
				data: "change=true&id="+playlistid,
				success: function(output) {
					output = $.parseJSON(output);
					$('#playlist tbody tr').each(function() {
						$(this).remove();
					});
					$('#player').remove();

					playlist.playlistCollection.reset();

	                for (var i = 0; i < output['playlist'].length; i++) {
	                    playlist.playlistCollection.add(new Video({
	                        title: output['playlist'][i].title,
	                        id: output['playlist'][i].id,
	                        uploaded: output['playlist'][i].uploaded,
	                        viewCount: output['playlist'][i].viewCount,
							domid: i
	                    }), { silent: true });
						
						
						var playlistRow = new PlaylistVideoItemView({model: playlist.playlistCollection.models[i]});
						$('#playlist tbody').append(playlistRow.render().el);
	                }				
				},
				error: function() {
					alert("Changing playlist.");
				}
			});
		}
	},

	newPlaylistClicked : function() {
		var newPlaylistView = new NewPlaylistView;
		newPlaylistView.show();
	},

	playlistsBlur : function() {
		$('#savedplaylists').tooltip('hide');
	},

	deletePlaylistClicked : function() {
		var playlistid = +$('#savedplaylists').val();
		if (loggedin) {
			$.ajax({
				type: "POST",
				url: "ajax/deleteplaylist.php",
				dateType: "json",
				data: 'playlist='+playlistid,
				success: function(output) {
					$('#savedplaylists option[value="'+playlistid+'"]').remove();
				},
				error: function() {
					alert("Error with deleting playlist.");
				}
			});
		}
	}
    
});
