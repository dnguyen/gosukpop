var PlaylistVideoItemView = Backbone.View.extend({
    tagName: "tr",
    template: _.template($('#playlist-template').html()),
    
    events: {
       "click #playlist td .title" : "play",
       "click .play" : "playClicked",
       "click .remove" : "removeClicked"
    },
    
    render: function() {
         $(this.el).attr('id', this.model.get('domid')).html(this.template(this.model.toJSON()));
         
         return this;
    },
    
    play: function(e) {
         $('#playlist tr#' + playlist.get('currentVideo')).css('background', '#FFF');
         var video = $(e.currentTarget).parent().parent().attr('id');
         playlist.set({ currentVideo: video });
    },
   
    playClicked: function(e) {
        $('#playlist tr#' + playlist.get('currentVideo')).css('background', '#FFF');
        var video = $(e.currentTarget).parent().parent().parent().parent().attr('id');
        playlist.set({ currentVideo: video });
    },
    
    removeClicked: function(e) {
        playlist.playlistCollection.remove(this.model);
        var selectedDomId = $(e.currentTarget).parent().parent().parent().parent().attr('id');
        $('#playlist tr#'+selectedDomId).remove();
	}
});
