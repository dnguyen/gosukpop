var MainVideoItemView = Backbone.View.extend({
   tagName: "tr",
   template: _.template($('#mainVideoItem-template').html()),
   
   events: {
      "click .expand" : "preview",
      "click .add" : "add"
   },
   
   initialize: function() {
   },
   
   render: function() {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
   },
   
   add : function(e) {
         this.model.set({domid: playlist.playlistCollection.length});
         
         playlist.playlistCollection.add(this.model);
         
         if (playlist.playlistCollection.models.length == 1) {
            playlist.set({ currentVideo: 0 });
         }
      if (!loggedin) {
         mainVideoList.videoCollection.remove(this.model);
         $(e.currentTarget).parent().parent().parent().remove();
      }
         

   },
   
   preview : function(e) {
      var container = $('td#'+this.model.get('id')).find('.videoPreview');
      
      if (container.is(":visible")) {
            container.find('iframe').remove();
			container.hide();
	  } else {
			container.append('<iframe width="560" height="315" src="http://www.youtube.com/embed/' + this.model.get('id') + '" frameborder="0" allowfullscreen></iframe>');
			container.show();
	  }
   }
});
